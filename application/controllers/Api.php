<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mdl_exam','exam');

	}
	public function index()
	{
		echo "r4pid exam";
	}
	
	public function organization() {
		$aOrgCEO = $this->exam->get_organizations_ceo();
		foreach ($aOrgCEO as $aCeos) {
			$aParentEmpDetailsArray = $this->exam->get_employee_details($aCeos['employeeNumber']);
			$aParentEmpDetails = $aParentEmpDetailsArray[0];
			$aOrgHierarchy = $this->exam->getOrganizationHierarchy($aCeos['employeeNumber']);
			
			// Populate the Organization Hierarchy by Employee Id
			$aOrganizationIds = $this->populate_org_hierarchy($aOrgHierarchy);
			
			if(is_array($aOrganizationIds)) {
				foreach ($aOrganizationIds as $nLv1EmployeeNumber => $aOrganizationIdsItems) {
					$aEmpDetails = $this->exam->get_employee_details($nLv1EmployeeNumber);
					$aLv1EmployeeDetails = $aEmpDetails[0];
					
					foreach ($aOrganizationIdsItems as $nLv2EmpNumber => $aLv3EmpNumberItems) {
						$aLv2EmpDetailsData = array();
						if(!empty($nLv2EmpNumber)) {
							$aLv2EmpDetails = $this->exam->get_employee_details($nLv2EmpNumber);	
							$aLv2EmpDetailsData = $aLv2EmpDetails[0];
							
							foreach ($aLv3EmpNumberItems as $nLv3EmpNumber => $aLv3Dummy) {
								$aLv3EmpDetailsData = array();
								if(!empty($nLv3EmpNumber)) {
									$aLv3EmpDetails = $this->exam->get_employee_details($nLv3EmpNumber);	
									$aLv3EmpDetailsData = $aLv3EmpDetails[0];
								}
								$aLv2EmpDetailsData['employeeUnder'][] = $aLv3EmpDetailsData;
							}
						}
						$aLv1EmployeeDetails['employeeUnder'][] = $aLv2EmpDetailsData;
					}
					$aOrganizationData[] = $aLv1EmployeeDetails;
				}
				$aParentEmpDetails['employeeUnder'] = $aOrganizationData;
			}
		}
		echo json_encode($aParentEmpDetails);
		exit();
	}
	
	private function populate_org_hierarchy($aOrgHierarchy) {
		if(isset($aOrgHierarchy) && is_array($aOrgHierarchy)) {
			foreach ($aOrgHierarchy as $aLevels) {
				$aOrganizationIds[$aLevels['lev1']][$aLevels['lev2']][$aLevels['lev3']] = '';
			}
			return $aOrganizationIds;
		} 
		return false;
	}
	
	public function offices() {
		echo "<pre>";
		$aGetOffices = $this->exam->get_offices();
		foreach ($aGetOffices as $aGetOfficesDetails) {
			$nOfficeCode = $aGetOfficesDetails['officeCode'];
			$aOfficesData['officeCode'] = $nOfficeCode;
			$aOfficesData['city'] = $aGetOfficesDetails['city'];
			$aGetEmpByOffices = $this->exam->get_employee_by_office($nOfficeCode);
			
			if(!$aGetEmpByOffices) {
				echo "No Employee Found";
			} else {
				
				foreach ($aGetEmpByOffices as $aEmps) {
					$aEmpInfo = array();
					$aEmpInfo['employeeNumber'] = $aEmps['employeeNumber'];
					$aEmpInfo['name'] = $aEmps['firstName']." ".$aEmps['lastName'];
					$aEmpInfo['jobTitle'] = $aEmps['jobTitle'];
					
					$aOfficesData['employees'][] = $aEmpInfo; 
				}
				
			}
			$aOfficesItems[] = $aOfficesData;
			unset($aOfficesData);
		}
//		print_r($aOfficesItems);
		echo json_encode($aOfficesItems);
		exit();
	}
	
	public function sales_report($nEmployeeNumber = null) {
		echo "<pre>";
		$aSalesReport = array();
		if(!empty($nEmployeeNumber)) {
			$aEmployeeDetails = $this->exam->get_employee_details($nEmployeeNumber);
		} else {
			$aEmployeeDetails = $this->exam->get_all_employees();
		}
		
		foreach ($aEmployeeDetails as $aEmpDetails) {
			$nEmployeeNumber = $aEmpDetails['employeeNumber'];
			$nMainTotalSales = 0;
			$nTotalCommisionPerProduct = 0;
			
			$aProductItemsDetails['employeeNumber'] = $aEmpDetails['employeeNumber'];
			$aProductItemsDetails['name'] = $aEmpDetails['firstName'].' '.$aEmpDetails['lastName'];
			$aProductItemsDetails['jobTitle'] = $aEmpDetails['jobTitle'];
			$aProductItemsDetails['officeCode'] = $aEmpDetails['officeCode'];
			
			$aProductItemsDetails['city'] = $aEmpDetails['city'];
			$aProductItemsDetails['totalCommision'] = 0;
			$aProductItemsDetails['totalSales'] = 0;

			$aEmployeeSalesInfo = $this->exam->get_employee_sales($nEmployeeNumber);
			if(is_array($aEmployeeSalesInfo)) {
			
				$aProductLinesData = $this->populateProductLines($aEmployeeSalesInfo);
				foreach ($aProductLinesData as $sProductLine => $sProductLineInfo) {
					$aProductLine['productLine'] = $sProductLine;
					$aProductLine['textDescription'] = $sProductLineInfo['textDescription'];
					$aProductLine['quantity'] = 0;
					$aProductLine['sales'] = 0;
	
					$nTotalQuantitySold = 0;
					$nTotalSales = 0;
					$nTotalCommision = 0;
					foreach ($sProductLineInfo['products'] as $sProductCode => $aProductInfo) {
						$aProductData['productCode'] = $sProductCode;
						$aProductData['productName'] = $aProductInfo['productName'];
						$aProductData['quantity'] = $aProductInfo['quantitySold']*1;
						
						$nTotalQuantitySold += $aProductInfo['quantitySold'];
						$aProductData['sales'] = $this->format_double($aProductInfo['sales']);
						$nTotalSales += $aProductInfo['sales'];
						$aProductData['numberOfCustomerBought'] = $aProductInfo['numberOfCustomerBought']*1;
						
						$nTotalCommisionPerProduct += $aProductInfo['commision'];
						
						$aProductLine['products'][] = $aProductData;
					}
					$aProductLine['quantity'] = $nTotalQuantitySold*1;
					$aProductLine['sales'] = $this->format_double($nTotalSales);
					
					$nMainTotalSales += $aProductLine['sales'];
					
					$aProductItemsDetails['productLines'][] = $aProductLine;
				}
				$aProductItemsDetails['totalCommision'] =  $this->format_double($nTotalCommisionPerProduct);
				$aProductItemsDetails['totalSales'] =  $this->format_double($nMainTotalSales);
			}
			$aSalesReport[] = $aProductItemsDetails;
		}
		echo json_encode($aSalesReport);
	}
	
	private function format_double($int) {
		return number_format( $int, 2, '.', '' )*1;
	}
	
	private function populateCustomerOrderInfo($nEmployeeNumber) {
		$aCustomers = $this->exam->get_customer_info_by_sales_rep($nEmployeeNumber);
		foreach ($aCustomers as $aCustomersItems) {
			$aCustomerInfo[$aCustomersItems['customerNumber']] = $aCustomersItems;

			$aOrderInfo = array();
			
			$aCustomerOrders = $this->exam->get_customer_orders($aCustomersItems['customerNumber']);
			foreach ($aCustomerOrders as $aCustomerOrdersItems) {
				$aOrderInfo[$aCustomerOrdersItems['orderNumber']] = $aCustomerOrdersItems;
				
				$aCustomerOrderDetails = $this->exam->get_order_details($aCustomerOrdersItems['orderNumber']);
				
				$aOrderInfo[$aCustomerOrdersItems['orderNumber']]['items'] = $aCustomerOrderDetails;
			}
			
			$aCustomerInfo[$aCustomersItems['customerNumber']]['order_details'] = $aOrderInfo;
		}
		
		return $aCustomerInfo;
	}
	
	private function populateProductLines($aEmployeeSalesInfo) {
		$aProductLinesData = false;
		foreach ($aEmployeeSalesInfo as $aEmployeeSalesInfoItems) {
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['productLines'] = $aEmployeeSalesInfoItems['productLine'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['textDescription'] = $aEmployeeSalesInfoItems['textDescription'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['products'][$aEmployeeSalesInfoItems['productCode']]['productScale'] = $aEmployeeSalesInfoItems['productScale'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['products'][$aEmployeeSalesInfoItems['productCode']]['commision'] = $aEmployeeSalesInfoItems['commision'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['products'][$aEmployeeSalesInfoItems['productCode']]['productName'] = $aEmployeeSalesInfoItems['productName'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['products'][$aEmployeeSalesInfoItems['productCode']]['quantityInStock'] = $aEmployeeSalesInfoItems['quantityInStock'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['products'][$aEmployeeSalesInfoItems['productCode']]['quantitySold'] = $aEmployeeSalesInfoItems['quantity'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['products'][$aEmployeeSalesInfoItems['productCode']]['sales'] = $aEmployeeSalesInfoItems['sales'];
			$aProductLinesData[$aEmployeeSalesInfoItems['productLine']]['products'][$aEmployeeSalesInfoItems['productCode']]['numberOfCustomerBought'] = $aEmployeeSalesInfoItems['numberOfCustomerBought'];
		}
		
		return $aProductLinesData;
	}
}


