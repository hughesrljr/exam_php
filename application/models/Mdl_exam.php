<?php
class Mdl_exam extends CI_Model {

    function __construct(){
        parent::__construct();
		$this->load->database();
    }
    /**
     * Get the Organization Heirarchy
     *
     * @param integer $nMainParentId
     * @return array
     */
    
    function getOrganizationHierarchy($nMainParentId) {
    	if(isset($nMainParentId)) {
	    	$query = $this->db->query('SELECT 
	    		t1.employeeNumber AS lev1, 
				t2.employeeNumber as lev2, 
				t3.employeeNumber as lev3, 
				t4.employeeNumber as lev4
				FROM employees AS t1
				LEFT JOIN employees AS t2 ON t2.reportsTo = t1.employeeNumber
				LEFT JOIN employees AS t3 ON t3.reportsTo = t2.employeeNumber
				LEFT JOIN employees AS t4 ON t4.reportsTo = t3.employeeNumber
				WHERE t1.reportsTo='.$nMainParentId);
	    	
	    	return $query->result_array();
    	}
    	return false;
    }
    function get_offices($officeCode="") {
    	$sCond = '';
    	if(isset($officeCode) && !empty($officeCode)) {
    		$sCond = ' WHERE officeCode='.$officeCode;
    	}
    	
    	$query = $this->db->query('SELECT * FROM `offices`'.$sCond);
    	return $query->result_array();
    }
    function get_employee_by_office($officeCode) {
    	if (isset($officeCode)) {
    		$query = $this->db->query('SELECT * FROM `employees` WHERE officeCode='.$officeCode);
    		return $query->result_array();
    	}
    	return false;
    }
    function get_all_employees() {
    	$query = $this->db->query('SELECT * FROM `employees` a INNER JOIN offices b ON a.officeCode=b.officeCode');
    	return $query->result_array();
    }
    function get_employee_details($employeeNumber) {
    	$query = $this->db->query('SELECT * FROM `employees` a INNER JOIN offices b ON a.officeCode=b.officeCode WHERE employeeNumber='.$employeeNumber." LIMIT 1");
    	return $query->result_array();
    }
    
    function get_organizations_ceo() {
    	$query = $this->db->query('SELECT * FROM `employees` WHERE reportsTo IS NULL OR reportsTo=""');
    	
    	return $query->result_array();
    }
    
    function get_customer_info_by_sales_rep($nSalesRepEmpNumber) {
    	if(isset($nSalesRepEmpNumber) && !empty($nSalesRepEmpNumber)) {
    		$query = $this->db->query('SELECT * FROM `customers` WHERE salesRepEmployeeNumber='.$nSalesRepEmpNumber);
    		return $query->result_array();
    	}
    	return false;
    }
    function get_productlines($sProductLine="") {
    	$sCond = null;
    	if(isset($sProductLine) && !empty($sProductLine)) {
    		$sCond = " WHERE productLine = '".$sProductLine."'";	
    	}
    	$query = $this->db->query('SELECT * FROM `productlines`'.$sCond);
		return $query->result_array();
    }
    
    function get_products($sProductLine="") {
    	$sCond = null;
    	if(isset($sProductLine) && !empty($sProductLine)) {
    		$sCond = " WHERE productLine = '".$sProductLine."'";	
    	}
    	$query = $this->db->query('SELECT * FROM `products`'.$sCond);
		return $query->result_array();
    }
    
//    function get_customer_order_info($nCustomerNumber, $nOrderNumber) {
//    	$query = $this->db->query('SELECT * FROM `orders` WHERE customerNumber='.$nCustomerNumber);
//    	if (isset($nCustomerNumber)) {
//    		$query = $this->db->query('SELECT * FROM `orders` WHERE customerNumber='.$nCustomerNumber);
//    		return $query->result_array();
//    	}
//    	return false;
//    }
    
    function get_customer_orders($nCustomerNumber) {
    	if (isset($nCustomerNumber)) {
    		$query = $this->db->query('SELECT * FROM `orders` WHERE customerNumber='.$nCustomerNumber);
    		return $query->result_array();
    	}
    	return false;
    }
    function get_order_details($nOrderNumber) {
    	if (isset($nOrderNumber)) {
    		$query = $this->db->query('SELECT * FROM `orderdetails` WHERE orderNumber='.$nOrderNumber);
    		return $query->result_array();
    	}
    	return false;
    }
    
    function get_employee_sales($nEmployeeNumber="") {
    	$query = $this->db->select('a.productLine,
    					prodlines.textDescription,
    					a.productCode,  
    					a.productName,
    					a.quantityInStock, 
    					a.productScale,
						SUM(b.quantityOrdered*b.priceEach) as sales, 
						b.orderNumber, SUM(b.quantityOrdered) as quantity, 
						COUNT(d.customerNumber) as numberOfCustomerBought,
						SUM(((b.quantityOrdered/SUBSTRING_INDEX(a.productScale, ":", -1))*SUBSTRING_INDEX(a.productScale, ":", 1))*b.priceEach) as commision')
						 ->from('products as a')
						 ->join('orderdetails as b','a.productCode = b.productCode')
						 ->join('productlines as prodlines','a.productLine = prodlines.productLine')
						 ->join('orders as c','b.orderNumber = c.orderNumber')
						 ->join('customers as d','c.customerNumber = d.customerNumber')
						 ->join('employees as e','d.salesRepEmployeeNumber = e.employeeNumber')
						 ->where('e.employeeNumber', $nEmployeeNumber)
						 ->group_by('a.productCode')
						 //->limit(3)
						 ->get();
//		 echo $this->db->last_query();
        $result =  $query->result_array();
		if(count($result) == 0) return FALSE;
		
		
		return $result;
    }
}
?>